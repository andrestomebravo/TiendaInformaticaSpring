package com.eciformacion.tiendainformatica.persistencia;

import com.eciformacion.tiendainformatica.mappers.FabricanteMapper;
import com.eciformacion.tiendainformatica.mappers.FabricanteProductoMapper;
import com.eciformacion.tiendainformatica.mappers.ProductoMapper;

public class GestorDatos {
	private FabricanteMapper fabricanteMapper;
	private ProductoMapper productoMapper;
	private FabricanteProductoMapper fabricanteProductoMapper;
	
	
	public FabricanteProductoMapper getFabricanteProductoMapper() {
		return fabricanteProductoMapper;
	}
	public void setFabricanteProductoMapper(FabricanteProductoMapper fabricanteProductoMapper) {
		this.fabricanteProductoMapper = fabricanteProductoMapper;
	}
	public FabricanteMapper getFabricanteMapper() {
		return fabricanteMapper;
	}
	public void setFabricanteMapper(FabricanteMapper fabricanteMapper) {
		this.fabricanteMapper = fabricanteMapper;
	}
	public ProductoMapper getProductoMapper() {
		return productoMapper;
	}
	public void setProductoMapper(ProductoMapper productoMapper) {
		this.productoMapper = productoMapper;
	}
	
	

}
