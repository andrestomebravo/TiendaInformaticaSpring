package com.eciformacion.tiendainformatica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eciformacion.tiendainformatica.persistencia.GestorDatos;
import com.eciformacion.tiendainformatica.pojos.Fabricante;
import com.eciformacion.tiendainformatica.pojos.Productos;

@RestController
@RequestMapping("/producto")
public class ApiProductoController {
	@Autowired
	private GestorDatos gestorDatos;
	
	public GestorDatos getGestorDatos() {
		return gestorDatos;
	}

	public void setGestorDatos(GestorDatos gestorDatos) {
		this.gestorDatos = gestorDatos;
	}
	@GetMapping
	public List getAllProductos(){
		return (List)gestorDatos.getProductoMapper().getProductos();
	}
	@GetMapping("/{id_producto}")
	public Productos getProducto(@PathVariable("id_producto") int id_producto) {
		return gestorDatos.getProductoMapper().getProducto(id_producto);
	}

	@PostMapping()
		public Productos postProductos(@RequestBody Productos producto){
			 gestorDatos.getProductoMapper().addProducto(producto);
	return producto;
	}
	
	@DeleteMapping("/{id_producto}")
	public String deleteProductos(@PathVariable("id_producto") int id_producto){
		 gestorDatos.getProductoMapper().deleteProducto(id_producto);
		 return "producto borrado";
	}
	
	@PutMapping()
	public Productos editProducto(@RequestBody Productos producto){
		 gestorDatos.getProductoMapper().editProducto(producto);
		 return producto;
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}