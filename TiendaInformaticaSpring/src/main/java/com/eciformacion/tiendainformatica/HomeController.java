package com.eciformacion.tiendainformatica;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.eciformacion.tiendainformatica.HomeController;
import com.eciformacion.tiendainformatica.persistencia.GestorDatos;
import com.eciformacion.tiendainformatica.pojos.Fabricante;
import com.eciformacion.tiendainformatica.pojos.Productos;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	@Autowired
	private GestorDatos gestorDatos;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@GetMapping("/listarProductos")
	public String listarProductos( Model model) {
	//	System.out.println("listarProductos");
		model.addAttribute("lista", gestorDatos.getProductoMapper().getProductos());
		
		
		
		return "lista";
	}
	
	@PostMapping("/addProductos")
		public String addProductos(@RequestParam ("codigo_articulo")String codigo_articulo,@RequestParam("fabricante")int fabricante,
				@RequestParam("nombre_articulo") String nombre_articulo,@RequestParam("precio") long precio, Model model) {
	//	System.out.println("a�adirProducto");
		Productos p=new Productos();
		p.setCodigo_articulo(codigo_articulo);
		p.setFabricante(fabricante);
		p.setNombre_articulo(nombre_articulo);
		p.setPrecio(precio);
	
		if(gestorDatos.getProductoMapper().addProducto(p)>0) {
			model.addAttribute("resultado", "Producto a�adido correctamente");
		}else
			model.addAttribute("resultado", "No se pudo a�adir el producto");
		
			
			return "operacion";
		}
	
	@PostMapping("/borrarProducto")
	public String borrarProducto(@RequestParam("idprueba")int id_producto, Model model) {
	//	System.out.println("delete");
		if(gestorDatos.getProductoMapper().deleteProducto(id_producto)>0) {
			model.addAttribute("resultado", "Producto borrado correctamente");
		}else
			model.addAttribute("resultado", "No se pudo borrar el producto");
		
		
		return "operacion";
	}
	
	@PostMapping("/editarProducto")
	public String editarProducto(@RequestParam ("codigo_articulo")String codigo_articulo,@RequestParam("fabricante")int fabricante,
			@RequestParam("nombre_articulo") String nombre_articulo,@RequestParam("precio") int precio, @RequestParam("idprueba")int id_producto,Model model) {
		System.out.println("edit");
		Productos p=new Productos();
		p.setCodigo_articulo(codigo_articulo);
		p.setFabricante(fabricante);
		p.setNombre_articulo(nombre_articulo);
		p.setPrecio(precio);
		p.setIdprueba(id_producto);
		System.out.println("creado");
		
		if(gestorDatos.getProductoMapper().editProducto( p)>0) {
			model.addAttribute("resultado", "Producto editado correctamente");
		}else
			model.addAttribute("resultado", "No se pudo editar el producto");
		
		
		return "operacion";
	}
	
	@GetMapping("/listarFabricantesProductos")
	public String listarFabricantesProductos(Model model) {
		model.addAttribute("lista", gestorDatos.getFabricanteProductoMapper().listarFabricantesProductos());
		//System.out.println(gestorDatos.getFabricanteProductoMapper().listarFabricantesProductos().get(0).toString());
		return "lista";
	}
	
	@GetMapping("/listarFabricantes")
	public String listarFabricantes(Model model) {
//		System.out.println("listarFabricantes");
		model.addAttribute("lista", gestorDatos.getFabricanteMapper().getFabricantes());
		
		return "lista";
	}
	@PostMapping("/verFabricanteProducto")
	public String verFabricanteProducto(@RequestParam ("idprueba") int id_producto, Model model) {
		int id_fabricante=gestorDatos.getProductoMapper().devolverIDfabricante(id_producto);
		model.addAttribute("lista", gestorDatos.getFabricanteMapper().getFabricanteProducto(id_fabricante));
		
		return "lista";
	}
	@PostMapping("/addFabricante")
	public String addFabricante(@RequestParam ("apellidos_fabricante")String apellidos_fabricante,
			@RequestParam("nombre_fabricante") String nombre_fabricante,@RequestParam("telefono_fabricante") String telefono_fabricante, Model model) {
//		System.out.println("a�adirFabricante");
		Fabricante f=new Fabricante();
		f.setApellidos_fabricante(apellidos_fabricante);
		f.setNombre_fabricante(nombre_fabricante);
		f.setTelefono_fabricante(telefono_fabricante);
	
		if(gestorDatos.getFabricanteMapper().addFabricante(f)>0) {
			model.addAttribute("resultado", "Fabricante a�adido correctamente");
		}else
			model.addAttribute("resultado", "No se pudo a�adir el fabricante");
		return "operacion";
		
	}
	@PostMapping("/verProductosFabricante")
	public String verProductosFabricante(@RequestParam ("codigo_fabricante") int idfabricante, Model model) {
	//	System.out.println("verProductosFabricante");
		model.addAttribute("lista", gestorDatos.getProductoMapper().getProductosFabricante(idfabricante));
		
		
		return "lista";
	}
	
	
}
