package com.eciformacion.tiendainformatica.pojos;

import java.io.Serializable;

public class Productos implements Serializable{

	

	private int fabricante,idprueba;
	private String codigo_articulo,nombre_articulo;
	private long precio;
	public int getFabricante() {
		return fabricante;
	}
	public void setFabricante(int fabricante) {
		this.fabricante = fabricante;
	}
	public int getIdprueba() {
		return idprueba;
	}
	public void setIdprueba(int idprueba) {
		this.idprueba = idprueba;
	}
	public String getCodigo_articulo() {
		return codigo_articulo;
	}
	public void setCodigo_articulo(String codigo_articulo) {
		this.codigo_articulo = codigo_articulo;
	}
	public String getNombre_articulo() {
		return nombre_articulo;
	}
	public void setNombre_articulo(String nombre_articulo) {
		this.nombre_articulo = nombre_articulo;
	}
	public long getPrecio() {
		return precio;
	}
	public void setPrecio(long precio) {
		this.precio = precio;
	}
	
	@Override
	public String toString() {
		return "Productos [fabricante=" + fabricante + ", idprueba=" + idprueba + ", codigo_articulo=" + codigo_articulo
				+ ", nombre_articulo=" + nombre_articulo + ", precio=" + precio + "]";
	}
	
	
}
