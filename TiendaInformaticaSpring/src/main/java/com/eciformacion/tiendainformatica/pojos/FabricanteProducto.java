package com.eciformacion.tiendainformatica.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class FabricanteProducto implements Serializable {
	private int codigo_fabricante;
	private String nombre_fabricante,apellidos_fabricante,telefono_fabricante;
	private List<Productos> productos;
	
	
	public int getCodigo_fabricante() {
		return codigo_fabricante;
	}


	public void setCodigo_fabricante(int codigo_fabricante) {
		this.codigo_fabricante = codigo_fabricante;
	}


	public String getNombre_fabricante() {
		return nombre_fabricante;
	}


	public void setNombre_fabricante(String nombre_fabricante) {
		this.nombre_fabricante = nombre_fabricante;
	}


	public String getApellidos_fabricante() {
		return apellidos_fabricante;
	}


	public void setApellidos_fabricante(String apellidos_fabricante) {
		this.apellidos_fabricante = apellidos_fabricante;
	}


	public String getTelefono_fabricante() {
		return telefono_fabricante;
	}


	public void setTelefono_fabricante(String telefono_fabricante) {
		this.telefono_fabricante = telefono_fabricante;
	}


	public List<Productos> getProductos() {
		return productos;
	}


	public void setProductos(List<Productos> productos) {
		this.productos = productos;
	}


	@Override
	public String toString() {
		String retorno= "FabricanteProducto [codigo_fabricante=" + codigo_fabricante + ", nombre_fabricante=" + nombre_fabricante
				+ ", apellidos_fabricante=" + apellidos_fabricante + ", telefono_fabricante=" + telefono_fabricante;
		for(int i=0; i<productos.size();i++) {
			retorno+="\n"+productos.get(i).toString();
			
		}
		return retorno;
	}


	



	

}
