package com.eciformacion.tiendainformatica.pojos;

import java.io.Serializable;

public class Fabricante implements Serializable{

	
	private int codigo_fabricante;
	private String nombre_fabricante,apellidos_fabricante,telefono_fabricante;
	public int getCodigo_fabricante() {
		return codigo_fabricante;
	}
	public void setCodigo_fabricante(int codigo_fabricante) {
		this.codigo_fabricante = codigo_fabricante;
	}
	public String getNombre_fabricante() {
		return nombre_fabricante;
	}
	public void setNombre_fabricante(String nombre_fabricante) {
		this.nombre_fabricante = nombre_fabricante;
	}
	public String getApellidos_fabricante() {
		return apellidos_fabricante;
	}
	public void setApellidos_fabricante(String apellidos_fabricante) {
		this.apellidos_fabricante = apellidos_fabricante;
	}
	public String getTelefono_fabricante() {
		return telefono_fabricante;
	}
	public void setTelefono_fabricante(String telefono_fabricante) {
		this.telefono_fabricante = telefono_fabricante;
	}
	@Override
	public String toString() {
		return "Fabricante [codigo_fabricante=" + codigo_fabricante + ", nombre_fabricante=" + nombre_fabricante
				+ ", apellidos_fabricante=" + apellidos_fabricante + ", telefono_fabricante=" + telefono_fabricante
				+ "]";
	}

	
}
