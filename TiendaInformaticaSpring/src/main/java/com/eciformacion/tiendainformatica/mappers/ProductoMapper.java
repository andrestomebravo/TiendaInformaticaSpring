package com.eciformacion.tiendainformatica.mappers;

import java.util.List;
import com.eciformacion.tiendainformatica.pojos.*;

public interface ProductoMapper {
	
	public List<Productos> getProductos();
	
	public Productos getProducto(int id_producto);
	
	public int addProducto(Productos producto);
	
	public int editProducto(Productos producto);
	
	public int deleteProducto(int id_producto);
	
	public int devolverIDfabricante(int id_producto);
	
	public List<Productos> getProductosFabricante(int id_fabricante);
	

}
