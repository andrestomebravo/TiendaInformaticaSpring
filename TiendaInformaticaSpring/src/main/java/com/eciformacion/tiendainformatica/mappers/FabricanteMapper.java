package com.eciformacion.tiendainformatica.mappers;

import java.util.List;

import com.eciformacion.tiendainformatica.pojos.Fabricante;
import com.eciformacion.tiendainformatica.pojos.Productos;

public interface FabricanteMapper {
	
	public List<Fabricante> getFabricantes();
	
	public List<Fabricante> getFabricanteProducto(int id);
	
	public int addFabricante(Fabricante fabricante);

}
