package com.eciformacion.tiendainformatica.mappers;

import java.util.List;

import com.eciformacion.tiendainformatica.pojos.FabricanteProducto;

public interface FabricanteProductoMapper {

	
	public List<FabricanteProducto> listarFabricantesProductos();
}
