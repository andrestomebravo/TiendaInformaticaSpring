package com.eciformacion.tiendainformatica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eciformacion.tiendainformatica.persistencia.GestorDatos;
import com.eciformacion.tiendainformatica.pojos.Fabricante;


@RestController
@RequestMapping("/fabricante")
public class ApiFabricanteController {
	@Autowired
	private GestorDatos gestorDatos;
	
	public GestorDatos getGestorDatos() {
		return gestorDatos;
	}

	public void setGestorDatos(GestorDatos gestorDatos) {
		this.gestorDatos = gestorDatos;
	}
	@GetMapping
	public List getAllFabricante(){
		return (List)gestorDatos.getFabricanteMapper().getFabricantes();
	}
	
	
	@GetMapping("/{nombre_fabricante}/{apellidos_fabricante}/{telefono_fabricante}")
	public Fabricante  addFabricante(@PathVariable ("apellidos_fabricante")String apellidos_fabricante,
									@PathVariable("nombre_fabricante") String nombre_fabricante,
									@PathVariable("telefono_fabricante") String telefono_fabricante){
		
		Fabricante f=new Fabricante();
		f.setApellidos_fabricante(apellidos_fabricante);
		f.setNombre_fabricante(nombre_fabricante);
		f.setTelefono_fabricante(telefono_fabricante);
		if(gestorDatos.getFabricanteMapper().addFabricante(f)>0) {
			return f;
		}else
			return null;
	}

	
	@PostMapping()
		public Fabricante  postFabricante(@RequestBody Fabricante fabricante){
			 gestorDatos.getFabricanteMapper().addFabricante(fabricante);
	return fabricante;	
	}
}
